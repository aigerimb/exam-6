const baseUrl = "http://146.185.154.90:8000/blog/blue@whale.in";
let lastDateTime;

const fetchData = (endpoint) => {
  return fetch(`${baseUrl}/${endpoint}`)
    .then(response => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error("Network error");
      }
    });
};

const postData = (endpoint, body) => {
  return fetch(`${baseUrl}/${endpoint}`, {
    method: "POST",
    body
  }).then(response => {
    if(response.ok) {
      return response.json();
    }
  });
};

const messageBox = document.getElementById("messageBox");

const renderProfile = (profile) => {
  document.getElementById("firstName").innerText = profile.firstName;
  document.getElementById("lastName").innerText = profile.lastName;
};

fetchData("profile").then(renderProfile);

const editButton = document.getElementById("editProfileBtn");
const editModal = document.getElementById("editModal");
const editForm = document.getElementById("editForm");

editButton.addEventListener("click", () => {
  editModal.style.display = "block";
});
editForm.addEventListener("submit", e => {
  var firstName = document.getElementById("editFirstName").value;
  var lastName = document.getElementById("editLastName").value;
  e.preventDefault();

  const data = new URLSearchParams({firstName, lastName});

  postData("profile", data)
    .then(renderProfile)
    .then(() => editModal.style.display = "none");
});

const renderMessages = (messages) => {
  messages = messages.slice(-20);
  messages.forEach(message => {
    const html = `
      <div class="message">
          <p class="datetime">${message.datetime}</p>
          <b class="user">${message.user.firstName}</b>: ${message.message}
      </div>
    `;
    const msgElement = document.createElement("div");
    msgElement.innerHTML = html;
    messageBox.insertBefore(msgElement, messageBox.firstChild);
  });
};

const getPosts = (datetime) => {
    fetchData(`posts${datetime ? "?datetime=" + lastDateTime : ""}`)
      .then(posts => {
        if (posts.length) {
          lastDateTime = posts[posts.length - 1].datetime;
          renderMessages(posts);
        }
    });
};

getPosts();
setInterval(() => {
  getPosts(lastDateTime);
}, 3000);

const messageForm = document.getElementById("messageForm");

messageForm.addEventListener("submit", e => {
  e.preventDefault();
  const value = document.getElementById("message").value;
  const data = new URLSearchParams();
  data.append("message", value);
  postData("posts", data).then(console.log);
});

const subscribeBtn = document.getElementById("subscribeBtn");
const subscribeModal = document.getElementById("subscribeModal");
const subscribeForm = document.getElementById("subscribeForm");
const subscribeEmail = document.getElementById("subscribeEmail");

subscribeBtn.addEventListener("click", () => {
  subscribeModal.style.display = "block";
});

subscribeForm.addEventListener("submit", e => {
  e.preventDefault();
  const email = subscribeEmail.value;
  const data = new URLSearchParams();
  data.append("email", email);
  postData("subscribe", data);
});